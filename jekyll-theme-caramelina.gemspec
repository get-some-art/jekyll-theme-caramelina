Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-caramelina"
  spec.version       = "0.2.2"
  spec.authors       = ["Andrey Ovcharov"]
  spec.email         = ["andrew.ovcharov@gmail.com"]

  spec.summary       = "Theme for caramelina.ru website"
  spec.homepage      = "https://caramelina.ru"
  spec.license       = "MIT"

  spec.metadata["plugin_type"] = "theme"

  spec.files = `git ls-files -z`.split("\x0").select do |f|
    f.match(%r!^(assets|_(includes|layouts|sass)/|(LICENSE|README)((\.(txt|md|markdown)|$)))!i)
  end

  spec.add_runtime_dependency "jekyll", ">= 4.0"
  spec.add_runtime_dependency "nokogiri"
  spec.add_runtime_dependency "jekyll-feed"
  spec.add_runtime_dependency "jekyll-seo"
  spec.add_runtime_dependency "jekyll-seo-tag"
  spec.add_runtime_dependency "jekyll-paginate-v2", ">= 3.0"
  spec.add_runtime_dependency "jekyll-responsive-image"
  spec.add_runtime_dependency "jekyll-sitemap"

  spec.add_development_dependency "bundler"
end
