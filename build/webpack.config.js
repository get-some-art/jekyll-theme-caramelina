const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const baseDir = path.resolve(__dirname, '..');

module.exports = {
    entry: {
        main: './src/js/main.js',
    },
    output: {
        path: path.resolve(baseDir, 'assets'),
        publicPath: '/assets/',
        filename: '[name].js',
        chunkFilename: '[chunkhash].js',
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[chunkhash].css',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.js/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.s?[ac]ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'postcss-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 4096,
                        },
                    },
                ],
            },
        ],
    },
    optimization: {
        splitChunks: {
            // maxSize: 200000,
            // maxAsyncRequests: 4,
            // maxInitialRequests: 4,
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all',
                },
            },
        },
    },

    resolve: {
        extensions: ['.js'],
    },
};
