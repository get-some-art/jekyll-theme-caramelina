const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');

const baseWebpackConfig = require('./webpack.config');

module.exports = merge(baseWebpackConfig, {
    mode: 'development',
    devtool: 'source-map',
    devServer: {
        port: 8080,
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        hot: true,
        historyApiFallback: true,
    },
    plugins: [
        // new webpack.NamedModulesPlugin(),
    ],
});
