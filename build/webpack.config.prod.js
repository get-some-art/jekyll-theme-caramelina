const path = require('path');
const glob = require('glob-all');
const webpack = require('webpack');
const { merge } = require('webpack-merge');

const PurgecssPlugin = require('purgecss-webpack-plugin');

const baseWebpackConfig = require('./webpack.config');

const baseDir = path.resolve(__dirname, '..');

const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

module.exports = merge(baseWebpackConfig, {
    mode: 'production',
    devtool: false,
    output: {
        path: path.resolve(baseDir, 'assets'),
    },
    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify('production'),
        }),
    ],
    optimization: {
        minimizer: [
            new CssMinimizerPlugin(),
        ],
    }
});
