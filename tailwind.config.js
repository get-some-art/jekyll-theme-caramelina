module.exports = {
purge: [
    './_site/**/*.html',
],
  theme: {
    extend: {
      colors: {
        'transparent': 'transparent',

        'black': "#000",

        'grey-darker': '#222',
        'grey-dark': '#555',
        'grey': '#888',
        'grey-light': '#ddd',
        'grey-lighter': '#eee',

        'white': "#fff",

        'accent': 'red',
      },
      maxWidth: {
        'column': '710px',
      },
      height: {
        '100': '25rem',
      },
    },
    screens: {
      'tablet': '640px',
      'desktop': '1024px',
    },
    container: {
      center: true,
    }
  },
  variants: {},
  plugins: []
}